<?php
header('Access-Control-Allow-Origin: *');
include '../funciones.php';
$body = file_get_contents('php://input');
$info = json_decode($body);
$sql = $info->sql;
$metodo = $info->metodo;
    
if(strtolower($metodo) === 'get'){
  echo getSQL($sql);
}else{
  echo putSQL($sql);
}