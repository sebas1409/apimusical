<?php
header('Access-Control-Allow-Origin: *');
include '../funciones.php';
$body = file_get_contents('php://input');
$venta = json_decode($body);
    
$idcampo = $venta->idcampo;
$campos = $venta->campos;
$valores = $venta->valores;
$sql = 'UPDATE artistas SET ';
    
if(count($campos) == count($valores)){
  for ($i=0; $i < count($campos); $i++) { 
    $sql .= $campos[$i] . '=' . validarDato($valores[$i]) . ', ';
  }
    
  $sql = substr($sql, 0, -2);
  $sql .= ' WHERE ' . $idcampo->nombre . ' = ' . $idcampo->valor;
  echo putSQL($sql);
}else{
  $arr = ['error' => true, 'msg' => 'Los campos y valores no coinciden!'];
  echo json_encode($arr, JSON_UNESCAPED_UNICODE);
}