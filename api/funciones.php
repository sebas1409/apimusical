<?php
  function conecta_bd(){		
$servidor = "localhost";
		$usuario = "root";
		$clave = "";
		$bd = "bdmusical";
		
		/*$servidor = "localhost";
		$usuario = "id8009990_root";
		$clave = "sebas1409";
		$bd = "id8009990_bdmusical";*/

		$conn=mysqli_connect($servidor,$usuario,$clave, $bd);
		mysqli_set_charset($conn, "utf8");
		
		if(mysqli_connect_errno()){
			echo mysqli_connect_error();
			exit(0);
		}
		
		return $conn;
  }

	function getSQL($sql){
		$res;
		$conn	=	conecta_bd();
		$rs = mysqli_query($conn, $sql);
		$array = array();
		if ($rs) {
			$array = array();
			while ($fila = mysqli_fetch_assoc($rs)) {	
				//$array[] = array_map('utf8_encode', $fila); //Cuando no se almacenan las tildes
				$array[] = array_map(null, $fila);
			}
			$resultado = ["ok"=>true, "sql"=>$sql, "data"=>$array];
			$res = json_encode($resultado, JSON_NUMERIC_CHECK);
		}else{
			$resultado = ["error" => true, "sql"=>$sql, "msg" => mysqli_error($conn)];
			$res = json_encode($resultado, JSON_NUMERIC_CHECK);
		}
		mysqli_close($conn);
		return $res;
	}

	function putSQL($sql){
		$res;
		$conn	=	conecta_bd();
		$rs = mysqli_query($conn, $sql);
		if ($rs) {
			$last_id = mysqli_insert_id($conn);
			$array = ["ok"=>true, "sql"=>$sql, "id"=>$last_id];
			$res = json_encode($array, JSON_NUMERIC_CHECK);
		}else{
			$array = ["error" => true, "sql"=>$sql, "msg" => mysqli_error($conn)];
			$res = json_encode($array, JSON_NUMERIC_CHECK);
			//echo mysqli_error($conn);
		}
		mysqli_close($conn);
		return $res;
	}

	function validarDato($dato){
		$numericos = ["integer", "double", "float"];
		$texto = ["string"];
		$funciones = ["now()"];
		$nulos = ["NULL"];
		$booleano = ["boolean"];
	
		$tipo_dato = gettype($dato);
	
		if(in_array($tipo_dato, $numericos)){
			return $dato;
		}
	
		if(in_array($tipo_dato, $texto)){
			if(in_array($dato, $funciones)){
				return $dato;
			}else{
				return "'" . $dato . "'";
			}
		}
	
		if(in_array($tipo_dato, $nulos)){
			return "null";
		}
	
		if(in_array($tipo_dato, $booleano)){
			return $dato === true ? 1 : 0;
		}
	}
?>